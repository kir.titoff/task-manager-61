package ru.t1.ktitov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.ktitov.tm.api.service.IPropertyService;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @NotNull
    @Override
    public String getApplicationHost() throws UnknownHostException {
        return InetAddress.getLocalHost().getHostName();
    }

    @NotNull
    @Value("#{environment['buildNumber']}")
    private String applicationVersion;

    @NotNull
    @Value("#{environment['developer']}")
    private String authorName;

    @NotNull
    @Value("#{environment['email']}")
    private String authorEmail;

    @NotNull
    @Value("#{environment['server.host']}")
    private String serverHost;

    @NotNull
    @Value("#{environment['server.port']}")
    private Integer serverPort;

    @NotNull
    @Value("#{environment['session.key']}")
    private String sessionKey;

    @NotNull
    @Value("#{environment['session.timeout']}")
    private Integer sessionTimeout;

    @NotNull
    @Value("#{environment['database.username']}")
    private String databaseUser;

    @NotNull
    @Value("#{environment['database.password']}")
    private String databasePassword;

    @NotNull
    @Value("#{environment['database.url']}")
    private String databaseUrl;

    @NotNull
    @Value("#{environment['database.driver']}")
    private String databaseDriver;

    @NotNull
    @Value("#{environment['database.dialect']}")
    private String databaseDialect;

    @NotNull
    @Value("#{environment['database.hbm2ddl']}")
    private String databaseHbm2ddlAuto;

    @NotNull
    @Value("#{environment['database.show_sql']}")
    private String databaseShowSql;

    @NotNull
    @Value("#{environment['database.format_sql']}")
    private String databaseFormatSql;

    @NotNull
    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;

    @NotNull
    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @NotNull
    @Value("#{environment['database.second_lvl_cache']}")
    private String secondLvlCache;

    @NotNull
    @Value("#{environment['database.factory_class']}")
    private String factoryClass;

    @NotNull
    @Value("#{environment['database.use_query_cache']}")
    private String useQueryCache;

    @NotNull
    @Value("#{environment['database.use_min_puts']}")
    private String useMinimalPuts;

    @NotNull
    @Value("#{environment['database.region_prefix']}")
    private String regionPrefix;

    @NotNull
    @Value("#{environment['database.config_file_path']}")
    private String providerFilePath;

}
