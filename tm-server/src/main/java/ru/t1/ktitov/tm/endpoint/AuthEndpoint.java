package ru.t1.ktitov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import ru.t1.ktitov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.ktitov.tm.dto.request.user.UserLoginRequest;
import ru.t1.ktitov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.ktitov.tm.dto.response.user.UserLoginResponse;
import ru.t1.ktitov.tm.dto.response.user.UserLogoutResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@WebService(endpointInterface = "ru.t1.ktitov.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) {
        @NotNull String token = authService.login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) {
        authService.logout(request.getToken());
        return new UserLogoutResponse();
    }

}
