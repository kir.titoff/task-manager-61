package ru.t1.ktitov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.ktitov.tm.dto.model.ProjectDTO;

import java.util.List;

@Repository
public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDTO> {

    @Nullable
    List<ProjectDTO> findByUserId(@NotNull final String userId);

    @Nullable
    @Query("SELECT p FROM ProjectDTO p WHERE p.id = :id AND p.userId = :userId")
    ProjectDTO findByIdAndUserId(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("id") final String id
    );

    @Nullable
    @Query("SELECT p FROM ProjectDTO p WHERE p.userId = :userId ORDER BY :orderBy")
    List<ProjectDTO> findAll(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("orderBy") final String orderBy
    );

    @Query("SELECT COUNT(1) = 1 FROM ProjectDTO p WHERE p.id = :id AND p.userId = :userId")
    Boolean existsByIdAndUserId(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("id") final String id
    );

}
