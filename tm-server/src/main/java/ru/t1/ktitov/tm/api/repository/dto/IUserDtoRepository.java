package ru.t1.ktitov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1.ktitov.tm.dto.model.UserDTO;

@Repository
public interface IUserDtoRepository extends IDtoRepository<UserDTO> {

    @Nullable
    UserDTO findByLogin(@NotNull final String login);

    @Nullable
    UserDTO findByEmail(@NotNull final String email);

}
