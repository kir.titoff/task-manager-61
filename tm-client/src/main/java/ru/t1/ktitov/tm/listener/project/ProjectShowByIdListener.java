package ru.t1.ktitov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.dto.request.project.ProjectGetByIdRequest;
import ru.t1.ktitov.tm.dto.model.ProjectDTO;
import ru.t1.ktitov.tm.event.ConsoleEvent;
import ru.t1.ktitov.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIdListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-show-by-id";

    @NotNull
    public static final String DESCRIPTION = "Show project by id";

    @Override
    @EventListener(condition = "@projectShowByIdListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.print("ENTER PROJECT ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectGetByIdRequest request = new ProjectGetByIdRequest(getToken());
        request.setProjectId(id);
        @Nullable final ProjectDTO project = projectEndpoint.getProjectById(request).getProject();
        showProject(project);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
